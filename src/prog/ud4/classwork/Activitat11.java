
package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat11 {
    public static void main(String[] args) {
        imprimeix('a', 20, 5);
    }
    
    public static void imprimeix(char caracter, int numVoltes, int numLinies) {
        for (int i = 0; i < numLinies; i++) {
            for (int j = 0; j < numVoltes; j++) {
                System.out.print(caracter);
            }
            System.out.println();
        }
    }
}
