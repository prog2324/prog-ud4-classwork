package prog.ud4.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat25 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Num1: ");
        String n1 = sc.next();
        System.out.print("Num2: ");
        String n2 = sc.next();
        
        int num1 = Integer.parseInt(n1);
        int num2 = Integer.parseInt(n2);
        
        int suma = num1 + num2;
        int producto = num1 * num2;
        System.out.printf("Suma %d y Producto %d\n", suma, producto);
        
        sc.close();
    }
}
