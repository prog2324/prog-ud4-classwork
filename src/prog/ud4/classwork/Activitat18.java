package prog.ud4.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat18 {

    final static int OPCION_SALUDAR = 1;
    final static int OPCION_COMER = 2;
    final static int OPCION_HABLAR = 3;
    final static int OPCION_SALIR = 4;
    final static int NUM_OPCIONS = 4;

    public static void main(String[] args) {
        veureMenu();

    }

    private static void veureMenu() {
        int opcioSeleccionada;
        do {
            mostrarOpcions();
            opcioSeleccionada = obtindreSeleccio();
            manejarOpcio(opcioSeleccionada);
        } while (opcioSeleccionada != OPCION_SALIR);
    }

    private static void mostrarOpcions() {
        System.out.printf("%d. Saludar\n", OPCION_SALUDAR);
        System.out.printf("%d. Comer\n", OPCION_COMER);
        System.out.printf("%d. Hablar\n", OPCION_HABLAR);
        System.out.printf("%d. Salir\n", OPCION_SALIR);
    }

    private static int obtindreSeleccio() {
        Scanner sc = new Scanner(System.in);
        do {
            System.out.printf("Introdueix opció [%d-%d]", 1, NUM_OPCIONS);
            if (sc.hasNextInt()) {
                return sc.nextInt();
            }
            sc.next();
            System.out.println("Has de seleccionar opcio vàlida");
        } while (true);
    }

    private static void manejarOpcio(int opcioSeleccionada) {
        switch (opcioSeleccionada) {
            case OPCION_COMER:
                System.out.println("Comer!!!");
                break;
            case OPCION_HABLAR:
                System.out.println("Hablar!!!");
                break;
            case OPCION_SALUDAR:
                System.out.println("Saludar!!!");
                break;
            case OPCION_SALIR:
                System.out.println("Adios!");
                break;
            default:
                System.out.println("Opcion no valida");
        }

    }
}
