package prog.ud4.classwork;

import java.util.Scanner;

public class Activitat4 {

    public static void main(String[] args) {
        
        System.out.println("El mayor es: " + mayorDe3());

    }

    public static int mayorDe3() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce num: ");
        int a = sc.nextInt();
        System.out.print("Introduce num: ");
        int b = sc.nextInt();
        System.out.print("Introduce num: ");
        int c = sc.nextInt();
        sc.close();
        
        // primera forma
        int mayor = a;
        if (b > mayor) {
            mayor = b;
        }
        if (c > mayor) {
            mayor = c;
        }
        return mayor;
        
        // segunda forma
//        if (a>=b && a>=c) {
//            return a;
//        }
//        else if (b>=a && b>=c) {
//            return b;
//        }
//        else {
//            return c;
//        }        

        // tercera forma
//        int mayor = (a > b) ? a : b;
//        mayor = (c > mayor) ? c : mayor;
//        return mayor;

        // otra forma más
//        return (a >= b && a >= c) ? a : (b >= a && b >= c) ? b : c;

    }

}
