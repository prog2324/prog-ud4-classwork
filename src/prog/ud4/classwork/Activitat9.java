/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat9 {

    public static void main(String[] args) {
        int m = obtenirElMajor(5, 3, 3, 1);
        System.out.println("Major: " + m);
    }
    
    public static int obtenirElMajor(int num1, int num2, int num3, int num4) {
        int major;
        
        major = num1;
        if (num2 >= major) {
            major = num2;
        }
        if (num3 >= major) {
            major = num3;
        }
        if (num4 >= major) {
            major = num4;
        }
        return major;
    }
}
