package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat24 {
    public static void main(String[] args) {
        String cadena = "Esto es un programa que transforma el texto cambiando todas vocales por la vocal a";
        String resultado = todoConA(cadena);
        System.out.println(resultado);
    }

    private static String todoConA(String cadena) {
          // primera solució
//        String resultado = cadena.replace("e", "a");
//        resultado = resultado.replace("i", "a");
//        resultado = resultado.replace("o", "a");
//        resultado = resultado.replace("u", "a");
        
          // segona solució
//        resultado = cadena.replaceAll("[eiou]", "a");
        
        // Tercera solució
        String resultado = "";
        for (int i = 0; i < cadena.length(); i++) {
            char c = cadena.charAt(i);
            if (c=='e' || c=='i' || c=='o' || c=='u') {
                resultado = resultado + 'a';
            }
            else {
                resultado = resultado + c;
            }
        }
        return resultado;
    }
}
