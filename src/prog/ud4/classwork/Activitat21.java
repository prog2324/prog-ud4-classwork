package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat21 {

    final static String SALUT1 = "Hola";
    final static String SALUT2 = "Hello";
    final static String SALUT3 = "Què tal";

    public static void main(String[] args) {
        String cad = "Hola";
        boolean esSalutacio = esSalutacio(cad);
        System.out.println("Es un salut: " + esSalutacio);
    }

    private static boolean esSalutacio(String cad) {
        if (cad.equalsIgnoreCase(SALUT1) || cad.equalsIgnoreCase(SALUT2) || cad.equalsIgnoreCase(SALUT3)) {
            return true;
        }
        return false;
    }
}
