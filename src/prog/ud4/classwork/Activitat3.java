package prog.ud4.classwork;


public class Activitat3 {
    public static void main(String[] args) {
        System.out.println("Tabla de multiplicar");
        mostrarTaulesMultiplicar();
    }
    
    public static void mostrarTaulesMultiplicar() {
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.printf("%d x %d = %d \n", i, j, i * j);
            }
            System.out.println("------");
        }
    }
}
