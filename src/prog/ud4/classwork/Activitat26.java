package prog.ud4.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat26 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nom:");
        String nom = sc.next();
        System.out.print("1er cognom:");
        String primerCognom = sc.next();
        System.out.print("2n cognom:");
        String segonCognom = sc.next();
        String nomComplet = nom + " " + primerCognom + " " + segonCognom;

        // Punt1
        System.out.println(nomComplet.toLowerCase());
        System.out.println(nomComplet.toUpperCase());
        System.out.println("Tam: " + nomComplet.length());

        // Punt2
        int ultimaPos = nomComplet.length() - 1;
        char ultimCaracter = nomComplet.charAt(ultimaPos);
        int vecesAparece = 0;
        for (int i = 0; i < nomComplet.length(); i++) {
            char caracter = nomComplet.charAt(i);
            if (caracter == ultimCaracter) {
                vecesAparece++;
            }
        }
        System.out.printf("La letra %s apareix %d vegades\n", ultimCaracter, vecesAparece);

        // Punt3
        if (nomComplet.length() >= 2) {
            System.out.println(nomComplet.substring(0, 2));
        } else {
            System.out.println("La cadena no es suficientemente grande");
        }

        // Punt4
//        String primeraLetraNom = nom.substring(0, 1);
//        String primeraLetra1Cognom = primerCognom.substring(0, 1);
//        String primeraLetra2Cognom = segonCognom.substring(0, 1);
//        String restoNom = nom.substring(1);
//        String resto1Cognom = primerCognom.substring(1);
//        String resto2Cognom = segonCognom.substring(1);
//
//        String nomCompletCamelCase
//                = primeraLetraNom.toUpperCase() + restoNom.toLowerCase() + " "
//                + primeraLetra1Cognom.toUpperCase() + resto1Cognom.toLowerCase() + " "
//                + primeraLetra2Cognom.toUpperCase() + resto2Cognom.toLowerCase();
//        System.out.println(nomCompletCamelCase);

        // Punt 4: opció utilitzant métode i reutilitzant codi. 
        String nomCamelCase = obtindreCadenaCamelCase(nom);
        String primerCognomCamelCase = obtindreCadenaCamelCase(primerCognom);
        String segonCognomCamelCase = obtindreCadenaCamelCase(segonCognom);
        String nomCompletCamelCase = nomCamelCase + " " + primerCognomCamelCase + " " + segonCognomCamelCase;
        System.out.println(nomCompletCamelCase);
        
        // Punt5
        System.out.println("***".concat(nomCompletCamelCase).concat("***"));
        System.out.println("***" + nomCompletCamelCase + "***");
       
        
        // Punt6
        String nomInvertit = invertirCadena(nomCompletCamelCase);
        System.out.println(nomInvertit);
        
         sc.close();
    }

    private static String invertirCadena(String cad) {
        String resultado = "";
        for (int i = cad.length()-1; i >= 0; i--) {
            resultado += cad.charAt(i);
        }
        return resultado;
    }

    private static String obtindreCadenaCamelCase(String cad) {
        String primerCaracter = cad.substring(0, 1).toUpperCase();
        String resto = cad.substring(1).toLowerCase();
        return primerCaracter + resto;
    }
}
