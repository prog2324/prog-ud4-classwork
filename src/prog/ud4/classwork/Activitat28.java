package prog.ud4.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat28 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StringBuilder cadena = new StringBuilder();
        
        // Demanem cadenes al usuari i concatenem a la variable cadena
        System.out.print("Cadena1: ");
        cadena.append(sc.next());
        cadena.append(" ");
        System.out.print("Cadena2: ");
        cadena.append(sc.next());
        cadena.append(" ");
        System.out.print("Cadena3: ");
        cadena.append(sc.next());

        // Peguem la volta a la cadena. Podriem haver utilitzat el métode de exercicis anteriors.
//        StringBuilder cadenaInvertida = new StringBuilder();
//        for (int i = cadena.length() - 1; i >= 0; i--) {
//            cadenaInvertida.append(cadena.charAt(i));
//        }
//        System.out.println(cadenaInvertida.toString());
        
        // O també, hi ha un métode en StringBuilder que ja ho fa per nosaltres
        StringBuilder cadenaInvertida = cadena.reverse();
        System.out.println(cadenaInvertida.toString());

        // Inserim la paraula 'Casa' abans de la ultima paraula
        // Per a aixó, cal buscar la posició amb lastIndexOf.
        int posicioCasa = cadenaInvertida.lastIndexOf(" ") + 1;
        cadenaInvertida.insert(posicioCasa, "Casa ");
        System.out.println(cadenaInvertida);

    }
}
