package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat10 {
    public static void main(String[] args) {
        imprimeix('a', 50);        
        System.out.println();
        
        imprimeix('b', 50);        
        System.out.println();
        
        // Curiositat: si quidrem al método imprimeix 10 voltes d'aquesta manera....
        for (int i = 1; i <= 10; i++) {
            imprimeix('*', i);
            System.out.println();
        }
    }
    
    public static void imprimeix(char caracter, int numVoltes) {
        for (int i = 0; i < numVoltes; i++) {
            System.out.print(caracter);
        }
    }
}
