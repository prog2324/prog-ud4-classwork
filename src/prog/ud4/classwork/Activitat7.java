package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat7 {
    public static void main(String[] args) {
        double resultat1 = obtenirMultiplicacio(4.0, 2.3, 1.0);
        double resultat2 = obtenirMultiplicacio(0, 3.5, 4.2);
        double resultat3 = obtenirMultiplicacio(1, 2.8, 3.6);
        // ...
    }    
    
    public static double obtenirMultiplicacio(double a, double b, double c) {
        double resultat = a * b * c;
        return resultat;
    }
}
