package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat23 {
    public static void main(String[] args) {
        String cadena = "HOLA";
        String primeraMitad = primeraMitad(cadena);
        System.out.println(primeraMitad);
    }
    
    private static String primeraMitad(String cadena) {
        int posMitad = cadena.length()/2;
        //return cadena.substring(0, posMitad);
        
        String resultado = "";
        for (int i = 0; i < posMitad; i++) {
            resultado += cadena.charAt(i);
        }
        return resultado;
    }
}
