package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Matematica {

    public static int suma(int a, int b) {
        return a + b;
    }

    public static int suma(int a, int b, int c) {
        return a + b + c;
    }

    public static boolean esPrim(long n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
