/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud4.classwork;

import java.util.Scanner;

public class Activitat1 {
    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        System.out.print("Dime tamany de la forma: ");
        int tam = teclat.nextInt();
        
        System.out.println("-- Dibujo de la forma --");
        dibujarForma(tam);
        System.out.println("-- Fin dibujo --");
        
        teclat.close();          
    }
    
    public static void dibujarForma(int tamany) {
        for (int i = 0; i < tamany; i++) {
            for (int j = 0; j < tamany; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
        
    }
}
