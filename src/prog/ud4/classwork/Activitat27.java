package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat27 {

    public static void main(String[] args) {
        String cad = "radar";

        if (esCapicuaVersion1(cad)) {
            System.out.printf("%s es capicua", cad);
        } else {
            System.out.printf("%s no es capicua", cad);
        }
        System.out.println();
    }

    private static boolean esCapicuaVersion1(String cad) {
        cad = cad.replace(" ", "");
        int tamCad = cad.length();

        // solución 1
//        for (int i = 0, j = tamCad-1; i < tamCad/2; i++, j--) {
//            if (cad.charAt(i)!=cad.charAt(j)) {
//                return false;
//            }            
//        }

        // solución 2
        for (int i = 0; i < tamCad / 2; i++) {
            if (cad.charAt(i) != cad.charAt(tamCad - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    private static boolean esCapicuaVersion2(String cad) {
        cad = cad.replace(" ", "");
        String cadInvertida = invertirCadena(cad);
        if (cad.equals(cadInvertida)) {
            return true;
        }
        return false;
    }

    private static String invertirCadena(String cad) {
        String resultado = "";
        for (int i = cad.length() - 1; i >= 0; i--) {
            resultado += cad.charAt(i);
        }
        return resultado;
    }

}
