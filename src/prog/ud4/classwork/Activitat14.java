package prog.ud4.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat14 {

    public static void main(String[] args) {
        int resultado1 = Matematica.suma(1, 2);
        System.out.println(resultado1);
        int resultado2 = Matematica.suma(1, 2, 3);
        System.out.println(resultado2);

        while (true) {
            long n = demanaNumero();
            boolean esPrim = Matematica.esPrim(n);
            String respuesta = esPrim? "SI": "NO";
            System.out.printf("El número %d es prim: %s \n", n, respuesta);            
        }

    }

    public static long demanaNumero() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introdueix num: ");
        long n = sc.nextLong();
        return n;
    }
}
