package prog.ud4.classwork;

import java.util.Scanner;

public class Activitat6 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce num: ");
        int n1 = sc.nextInt();
        System.out.print("Introduce num: ");
        int n2 = sc.nextInt();
        System.out.print("Introduce num: ");
        int n3 = sc.nextInt();
        sc.close();
        System.out.println("El mayor es: " + majorDe3(n1, n2, n3));

    }

    public static int majorDe3(int a, int b, int c) {
        // Varias formas:
        
        // primera forma
        int major=  a;
        if (b > major) {
            major = b;
        }
        if (c > major) {
            major = c;
        }
        return major;
        
        // segunda forma
//        if (a>=b && a>=c) {
//            return a;
//        }
//        else if (b>=a && b>=c) {
//            return b;
//        }
//        else {
//            return c;
//        }        

        // tercera forma
//        int major = (a > b) ? a : b;
//        major = (c > major) ? c : major;
//        return major;

        // otra forma más
//        return (a >= b && a >= c) ? a : (b >= a && b >= c) ? b : c;

    }

}
