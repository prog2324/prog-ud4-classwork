package prog.ud4.classwork;

public class Activitat5 {

    public static void main(String[] args) {
        int m = 4;
        System.out.println("Tabla de multiplicar del " + m);
        mostrarTaulaMultiplicar(m);
    }

    public static void mostrarTaulaMultiplicar(int multiplicant) {
        for (int j = 1; j <= 10; j++) {
            System.out.printf("%d x %d = %d \n", multiplicant, j, multiplicant * j);
        }
        System.out.println("------");
    }
}
