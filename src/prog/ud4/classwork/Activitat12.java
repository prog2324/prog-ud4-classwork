package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat12 {

    public static void main(String[] args) {

        imprimieixPiramide('x', 10);
        
        imprimieixPiramide1('x', 10);

    }

    // Forma1: utilitzant el método imprimeix creat previament.
    public static void imprimieixPiramide(char caracter, int numLineas) {
        for (int nLinea = 1; nLinea <= numLineas; nLinea++) {
           
            int cantitatEspaisBlanc = numLineas - nLinea;
            imprimeix(' ', cantitatEspaisBlanc);
            
            int cantitatCaracters = nLinea * 2 - 1;
            imprimeix(caracter, cantitatCaracters);
            
            System.out.println();
        }
    }

    public static void imprimeix(char caracter, int numVoltes) {
        for (int i = 0; i < numVoltes; i++) {
            System.out.print(caracter);
        }
    }

    // Forma2
    public static void imprimieixPiramide1(char caracter, int numLineas) {
        for (int nLinea = 0; nLinea < numLineas; nLinea++) {
            
            int cantitatEspaisBlanc = numLineas - nLinea;
            for (int j = 0; j < cantitatEspaisBlanc; j++) {
                System.out.print(" ");
            }
            
            int cantitatCaracters = nLinea * 2 - 1;
            for (int j = 0; j < cantitatCaracters; j++) {
                System.out.print(caracter);
            }
            
            System.out.println();
        }
    }
}
