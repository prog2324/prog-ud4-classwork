package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat22 {

    public static void main(String[] args) {
        String cad = "Hola món";
        incorporaGuionet(cad);
    }

    private static void incorporaGuionet(String cad) {
        for (int i = 0; i < cad.length(); i++) {            
            if (cad.charAt(i)==' ') {
                continue;
            }
            System.out.print(cad.charAt(i));
            if (i < cad.length() - 1) {
                System.out.print("-");
            }
           
        }
    }

}
