package prog.ud4.classwork;

/**
 *
 * @author sergio
 */
public class Activitat29 {

    final static char BLOQUE1_INI = ' ';
    final static char BLOQUE1_FIN = '/';
    final static char BLOQUE2_INI = ':';
    final static char BLOQUE2_FIN = '@';
    final static char BLOQUE3_INI = '[';
    final static char BLOQUE3_FIN = '_';
    final static char BLOQUE4_INI = '{';
    final static char BLOQUE4_FIN = '}';

    public static void main(String[] args) {
        StringBuilder cad = new StringBuilder("hola! * / { que tal? bien!_[hello]");
        int numCaractersEspecials = comptaNumCaractersEspecials(cad);
        System.out.println("Total caracters especials: " + numCaractersEspecials);
    }

    private static int comptaNumCaractersEspecials(StringBuilder cadena) {
        int compta = 0;
        for (int i = 0; i < cadena.length(); i++) {
            if ((cadena.charAt(i) >= BLOQUE1_INI && cadena.charAt(i) <= BLOQUE1_FIN)
                    || (cadena.charAt(i) >= BLOQUE2_INI && cadena.charAt(i) <= BLOQUE2_FIN)
                    || (cadena.charAt(i) >= BLOQUE3_INI && cadena.charAt(i) <= BLOQUE3_FIN)
                    || (cadena.charAt(i) >= BLOQUE4_INI && cadena.charAt(i) <= BLOQUE4_FIN)) {
                compta++;
            }
        }
        System.out.println();
        return compta;
    }

}
